const initialState = {
    isLogged: false,
    user: {
        id: null,
        name: ''
    }
};

export default function (state = initialState, action) {
    switch(action.type) {
        case 'LOGIN':
            return {
                ...state,
                isLogged: true,
                user: {
                    id: action.payload.id,
                    name: action.payload.login
                }
            };
        case 'LOGOUT':
            return {
                ...state,
                isLogged: false,
                user: ''
            };
        default:
            return state;
    }
}

