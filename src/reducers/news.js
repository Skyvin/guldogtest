const initialState = {
    filter: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_FILTER':
            return {
                ...state,
                filter: action.payload
            };
        default:
            return state;
    }
}
