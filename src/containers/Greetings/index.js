import React, { Component } from 'react';
import { connect } from 'react-redux';

class Greetings extends Component {
    getUserName() {
        return this.props.auth.isLogged ? this.props.auth.user.name : 'Гость';
    }

    render() {
        return (
          <div className="container">
              <h2 className="text-center">Привет, {this.getUserName()}!</h2>
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(Greetings);
