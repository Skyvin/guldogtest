import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NEWS } from '../../constants/index';

import Post from '../../components/Post';

class News extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allNews: [],
        };

        this.changeFilter = this.changeFilter.bind(this);
    }

    componentDidMount() {
        this.loadNews();
    }

    loadNews() {
        const news = NEWS;

        this.setState({
            allNews: news,
        })
    }

    renderNews() {
        const filteredNews = this.getFilteredNews();
        return filteredNews.map((newsItem) => <Post key={newsItem.id} label={newsItem.label} text={newsItem.text} /> )
    }

    getFilteredNews() {
        const filterStr = this.props.news.filter.toLowerCase();
        let filteredNews = [];
        if(filterStr) {
            filteredNews = this.state.allNews.filter(post => post.label.toLowerCase().includes(filterStr));
        } else {
            filteredNews = this.state.allNews;
        }

        return filteredNews;
    }

    changeFilter(e) {
        const filterStr = e.target.value;
        this.props.onChangeFilter(filterStr);
    }

    render() {
        return (
            <div className="container">
                <input type="text" className="form-control" placeholder="Фильтрация" value={this.props.news.filter} onChange={this.changeFilter} />
                <div className="row mt-3">
                    { this.renderNews() }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        news: state.news
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onChangeFilter: filter => dispatch({ type: 'CHANGE_FILTER', payload: filter })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(News);
