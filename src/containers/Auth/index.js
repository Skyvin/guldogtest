import React, { Component } from 'react';
import { connect } from 'react-redux';
import { USERS } from '../../constants/';
import { Redirect } from 'react-router-dom';

class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            login: '',
            password: '',
            redirect: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    handleInputChange(e) {
        this.setState({ login: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    checkAuthData({ login, password }) {
        const registredUsers = USERS;

        const existUser = registredUsers.find((user) => {
            return user.login === login && user.password === password;
        });

        return existUser;
    }

    handleSubmit(e) {
        e.preventDefault();

        const logingData = {
            login: this.state.login,
            password: this.state.password
        };

        const existUser = this.checkAuthData(logingData);

        if(existUser) {
            this.props.onLogin(existUser);
            this.setState({redirect: true});
        } else{
            alert('Пользователь с такими логин/пароль не найден');
        }
    }

    renderDefaultUsers() {
        const usersElems = USERS.map((user) =>
            <div key={user.id} className="col-6 border border-primary m-1 rounded">
                <div>login: <strong>{user.login}</strong> </div>
                <div>password: <strong>{user.password}</strong></div>
            </div>
        );

        return usersElems;
    }

    render() {
        if(this.state.redirect) { return <Redirect to="/" /> }

        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-4 ">
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="username">Логин</label>
                                <input type="text" className="form-control" onChange={this.handleInputChange} value={this.state.login} name="username"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="password">Пароль</label>
                                <input type="password" className="form-control" onChange={this.handlePasswordChange} value={this.state.password} name="password"/>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-info">Войти</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-12 text-center">
                        <h3>Существующие пользователи</h3>
                    </div>
                    <div className="col-12 text-center">
                        <div className="row justify-content-center">
                            { this.renderDefaultUsers() }
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onLogin: userData => { dispatch({ type: 'LOGIN', payload: userData }) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
