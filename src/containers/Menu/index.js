import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustomLink from '../../components/CustomLink';

import './Menu.css';

class Menu extends Component {

    logOut(event){
        event.preventDefault();
        this.props.onLogOut();
    }

    getLogInOutLink() {
        if (this.props.auth.isLogged) {
            return (<CustomLink label='Выход' isLogout="true" onClick={this.logOut.bind(this)}/>)
        } else {
            return (<CustomLink label='Вход' to="/login" />)
        }
    }

    render() {
        return (
            <div className="Menu">
                <ul className="nav justify-content-center">
                    <CustomLink isExact={true} label='Главная' to="/"/>
                    <CustomLink label='Новости' to="/news"/>
                    {this.getLogInOutLink()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onLogOut: () => { dispatch({ type: 'LOGOUT' }) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
