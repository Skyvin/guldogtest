import React from 'react';
import { Route, Link } from 'react-router-dom';
import './CustomLink.css';

function CustomLink({ label, to, isExact, isLogout, onClick }){
    if (isLogout) {
        return (
            <li className="nav-item">
                <button className="nav-link" onClick={onClick}>
                    {label}
                </button>
            </li>
        );
    } else {
        return (
            <Route
                path={to}
                exact={isExact}
                children={({match}) => (
                    <li className={match ? 'nav-item active' : 'nav-item'}>
                        <Link to={to} className="nav-link" >{label}</Link>
                    </li>
                )}
            />
        )
    }
}

export default CustomLink;
