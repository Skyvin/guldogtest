import React, { Component } from 'react';

class Post extends Component {
    render() {
        return (
            <div className="col-md-4 col-sm-12">
                <div className="h3">{this.props.label}</div>
                <p>{this.props.text}</p>
            </div>
        );
    }
}

export default Post;
