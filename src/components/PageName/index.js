import React, { Component } from 'react';

class PageName extends Component {
    render() {
        return (
            <h1 className="text-center mb-5">{this.props.label}</h1>
        )
    }
}

export default PageName;
