import React from 'react';

import PageName from '../../components/PageName';
import News from '../../containers/News';

export default () => {
    return (
        <>
            <PageName label="Страница новостей" />
            <News />
        </>
    );
}
