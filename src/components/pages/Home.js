import React, { Component } from 'react';

import Greetings from '../../containers/Greetings';
import PageName from '../../components/PageName';

class Home extends Component {
    render() {
        return (
            <div className="App">
                <PageName label="Главная страница" />
                <Greetings/>
            </div>
        );
    }
}

export default Home;
