import React, { Component } from 'react';

import PageName from '../../components/PageName';
import Auth from '../../containers/Auth';

class Login extends Component{
    render() {
        return (
            <>
                <PageName label="Страница авторизации" />
                <Auth />
            </>
        );
    }
}

export default Login;
